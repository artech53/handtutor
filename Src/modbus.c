/*
 * modbus.c
 *
 *  Created on: Aug 1, 2019
 *      Author: maxim
 */

#include "main.h"
#include "string.h"

extern UART_HandleTypeDef huart3;
extern TIM_HandleTypeDef htim7;

extern uint8 own_ID;

uint8 reciveByte = 0;
bool FrameEnd = 0;

uint8 cnt = 0;
volatile uint8 TX_buffer[TX_buffer_length];
volatile uint8 RX_buffer[RX_buffer_length];

uint8 coils[no_of_coils] = {0};
uint8 discrete_inputs[no_of_inputs] = {0};
uint16 input_registers[no_of_input_regs] = {0};
uint16 holding_registers[no_of_holding_regs] = {0};

// возвращает указатель на HOLDING регистры
uint16* GetHoldingRegisters() {
	return holding_registers;
}

bool isNormalWork(){
	return FrameEnd;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {

}

// срабатывает каждый раз при получении данных по линии уарт
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	// запоминаем принятый байт данных
	RX_buffer[cnt++] = reciveByte;

	// сбрасываем таймер ожидания окончания посылки
//	HAL_TIM_Base_Stop_IT(&htim7);
	htim7.Instance->CNT = 0;
	HAL_TIM_Base_Start_IT(&htim7);

	// запускаем новую итерацию чтения байта данных
	HAL_UART_Receive_IT(huart, &reciveByte, 1);
}

// ициализация реистров
// ID - идентификатор изделия
void HoldingsRegistersInit(uint16 ID) {

	for(uint8 index = 0; index < no_of_holding_regs; index++) {
		holding_registers[index] = 0;
	}

	holding_registers[19] = ID;
}

void FrameSet(bool value) {
	FrameEnd = value;
}

bool FrameGet() {
	return FrameEnd;
}

void flush_RX_buffer(void)
{
    signed char i = (RX_buffer_length - 1);

    while(i > -1)
    {
        RX_buffer[i--] = 0x00;
    };

    cnt = 0x00;
}


void flush_TX_buffer(void)
{
    signed char i = (TX_buffer_length - 1);

    while(i > -1)
    {
        TX_buffer[i--] = 0x00;
    };
}


uint16 MODBUS_RTU_CRC16(uint8 *data_input, uint8 data_length)
{
    uint8 n = 0x08;
    uint8 s = 0x00;
    uint16 CRC_word = 0xFFFF;

    for(s = 0x00; s < data_length; s++)
    {
        CRC_word ^= ((uint16)data_input[s]);

        n = 8;

        while(n > 0)
        {
            if((CRC_word & 0x0001) == 0)
            {
              CRC_word >>= 1;
            }

            else
            {
              CRC_word >>= 1;
              CRC_word ^= 0xA001;
            }

            n--;
      }
    }

    return CRC_word;
}

void MODBUS_start() {

	flush_RX_buffer();
	flush_TX_buffer();

	HAL_TIM_Base_Stop_IT(&htim7);
	ReciveENA();
	// запускаем новую итерацию чтения байта данных
	HAL_UART_Receive_IT(&huart3, &reciveByte, 1);

	FrameEnd = 0;

//	HAL_GPIO_WritePin(D3_GPIO_Port, D3_Pin, GPIO_PIN_SET);
}

void MODBUS_receive_task(void)
{


     uint8 error_flag = 0;

     uint8 low_byte = 0x00;
     uint8 high_byte = 0x00;

     uint16 temp1 = 0x0000;
     uint16 temp2 = 0x0000;
     uint16 temp3 = 0x0000;
     uint16 temp4 = 0x0000;
     uint16 temp5 = 0x0000;
     uint16 temp6 = 0x0000;
     uint16 temp7 = 0x0000;

     static uint8 data_array[21] = {0};

     error_flag = 0;

     // проверяем остановку таймера ожидания окончания посылки (>3,5 длительностей байта)
     if(FrameEnd) {
    	 FrameEnd = 0;
		 if(RX_buffer[id_byte] == own_ID)
		 {
			 switch(RX_buffer[function_code_byte])
			 {
				 case FC_read_coils:
				 {
					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_coil_start) && (temp1 <= addr_coil_end))
					 {
						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);

						 if((temp2 <= no_of_coils) && (temp2 + temp1 - 1) <= addr_coil_end)
						 {
							 temp3 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
							 temp4 = MODBUS_RTU_CRC16(RX_buffer, 6);

							 if(temp4 == temp3)
							 {
								 temp4 = 0x0000;
								 temp5 = ((temp1 - addr_coil_start) + temp2 - 1);

								 while(temp2)
								 {
									 temp4 <<= 1;
									 temp4 |= coils[temp5];
									 temp5--;
									 temp2--;
								 };

								 data_array[0] = 0x01;
								 data_array[1] = ((uint8)temp4);

								 MODBUS_send_task(FC_read_coils, 2, data_array);
							 }

							 else
							 {
								 error_flag = 1;
							 }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_read_coils | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_read_discrete_inputs:
				 {
					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_input_start) && (temp1 <= addr_input_end))
					 {
						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);

						 if((temp2 <= no_of_inputs) && (temp2 + temp1 - 1) <= addr_input_end)
						 {
							 temp3 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
							 temp4 = MODBUS_RTU_CRC16(RX_buffer, 6);

							 if(temp4 == temp3)
							 {
								 temp4 = 0x0000;
								 temp5 = ((temp1 - addr_input_start) + temp2 - 1);

								 while(temp2)
								 {
									 temp4 <<= 1;
									 temp4 |= discrete_inputs[temp5];
									 temp5--;
									 temp2--;
								 };

								 data_array[0] = 0x01;
								 data_array[1] = ((uint8)temp4);

								 MODBUS_send_task(FC_read_discrete_inputs, 2, data_array);
							  }

							  else
							  {
								  error_flag = 1;
							  }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_read_discrete_inputs | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_read_holding_registers:
				 {// мы приняли полную полную посылку и теперь её надо отработать.

					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_holding_reg_start) && (temp1 <= addr_holding_reg_end))
					 {// если верный адрес регистра

						 // опеделяем адрес конца запрашиваемых данных
						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);

						 if((temp2 <= no_of_holding_regs) && (temp2 + temp1 - 1) <= addr_holding_reg_end)
						 {// если адреса - НОРМ - проверяем CRC
							 temp3 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
							 temp4 = MODBUS_RTU_CRC16(RX_buffer, 6);

							 if(temp4 == temp3)
							 {// если CRC - НОРМ - фигачм данные
								 data_array[0] = temp2;
								 data_array[0] <<= 1;

								 if(temp2 > 1)
								 {

									 for(temp3 = (temp1 - addr_holding_reg_start); temp3 < temp2; temp3++)
									 {
										 get_HB_LB(holding_registers[((temp1 - addr_holding_reg_start) + temp3)], &high_byte, &low_byte);
										 data_array[1 + temp3 + temp3] = high_byte;
										 data_array[2 + temp3 + temp3] = low_byte;
									 }
								 }

								 else
								 {
										 get_HB_LB(holding_registers[(temp1 - addr_holding_reg_start)], &high_byte, &low_byte);
										 data_array[1] = high_byte;
										 data_array[2] = low_byte;
								 }

								 MODBUS_send_task(FC_read_holding_registers, ((temp2 << 1) + 1), data_array);
							 }

							 else
							 {
								 error_flag = 1;
							 }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_read_holding_registers | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_read_input_registers:
				 {
					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_input_reg_start) && (temp1 <= addr_input_reg_end))
					 {
						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);

						 if((temp2 <= no_of_input_regs) && (temp2 + temp1 - 1) <= addr_input_reg_end)
						 {
							 temp3 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
							 temp4 = MODBUS_RTU_CRC16(RX_buffer, 6);

							 if(temp4 == temp3)
							 {
								 data_array[0] = temp2;
								 data_array[0] <<= 1;

								 if(temp2 > 1)
								 {

									 for(temp3 = (temp1 - addr_input_reg_start); temp3 < temp2; temp3++)
									 {
										 get_HB_LB(input_registers[((temp1 - addr_input_reg_start) + temp3)], &high_byte, &low_byte);
										 data_array[1 + temp3 + temp3] = high_byte;
										 data_array[2 + temp3 + temp3] = low_byte;
									 }
								 }

								 else
								 {
										 get_HB_LB(input_registers[(temp1 - addr_input_reg_start)], &high_byte, &low_byte);
										 data_array[1] = high_byte;
										 data_array[2] = low_byte;
								 }

								 MODBUS_send_task(FC_read_input_registers, ((temp2 << 1) + 1), data_array);
							 }

							 else
							 {
								 error_flag = 1;
							 }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_read_input_registers | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_write_single_coil:
				 {
					 temp2 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp2 >= addr_coil_start) && (temp2 <= addr_coil_end))
					 {
						  temp1 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
						  temp3 = MODBUS_RTU_CRC16(RX_buffer, 6);

						  if(temp1 == temp3)
						  {
							  temp1 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);

							  if(temp1 == coil_ON)
							  {
								  coils[temp2] = ON;
							  }

							  else if(temp1 == coil_OFF)
							  {
								  coils[temp2] = OFF;
							  }

							  data_array[0] = RX_buffer[location_start_high_byte];
							  data_array[1] = RX_buffer[location_start_low_byte];
							  data_array[2] = RX_buffer[location_end_high_byte];
							  data_array[3] = RX_buffer[location_end_low_byte];

							  MODBUS_send_task(FC_write_single_coil, 4 , data_array);
						  }

						  else
						  {
							  error_flag = 1;
						  }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_write_single_coil | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_write_single_register:
				 {
					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_holding_reg_start) && (temp1 <= addr_holding_reg_end))
					 {
						 temp3 = make_word(RX_buffer[CRC_low_byte], RX_buffer[CRC_high_byte]);
						 temp4 = MODBUS_RTU_CRC16(RX_buffer, 6);

						 if(temp4 == temp3)
						 {// если приняли всё норм - извлекаем данные/параметры
							 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);
							 holding_registers[temp1 - addr_holding_reg_start] = temp2;

							 data_array[0] = RX_buffer[location_start_high_byte];
							 data_array[1] = RX_buffer[location_start_low_byte];
							 data_array[2] = RX_buffer[location_end_high_byte];
							 data_array[3] = RX_buffer[location_end_low_byte];

							 MODBUS_send_task(FC_write_single_register, 4 , data_array);

						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_write_single_register | 0x80), 1, data_array);
						   break;
						 }
						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_write_multiple_coils:
				 {
					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((temp1 >= addr_coil_start) && (temp1 <= addr_coil_end))
					 {
						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);
						 temp3 = (temp2 + temp1 - 1);

						 if((temp2 <= no_of_coils) && (temp3 <= addr_coil_end))
						 {
							 temp3 = RX_buffer[byte_size_byte];

							 if((temp2 > 0) && (temp2 <= 8))
							 {
								 temp4 = 1;
							 }
							 else if((temp2 > 8) && (temp2 <= 16))
							 {
								 temp4 = 2;
							 }

							 if(temp3 == temp4)
							 {
								 temp7 = (mandatory_bytes_to_read + temp4);

								 temp5 = make_word(RX_buffer[(1 + temp7)], RX_buffer[temp7]);
								 temp6 = MODBUS_RTU_CRC16(RX_buffer, temp7);

								 if(temp6 == temp5)
								 {
									 if(temp3 == 1)
									 {
										 temp4 = RX_buffer[(temp7 - 1)];
										 temp5 = (temp1 - addr_coil_start);

										 if(temp2 > 1)
										 {
											 for(temp6 = temp5; temp6 < (temp2 + temp5); temp6++)
											 {
												 coils[temp6] = (temp4 & 0x01);
												 temp4 >>= 1;
											 }
										 }

										 else
										 {
											 coils[temp5] = temp4;
										 }
									 }

									 else
									 {
										 temp4 = make_word(RX_buffer[(temp7 - 1)], RX_buffer[temp7]);

										 for(temp6 = temp5; temp6 < (temp2 + temp5); temp6++)
										 {
											 coils[temp6] = (temp4 & 0x01);
											 temp4 >>= 1;
										 }
									 }

									 data_array[0] = RX_buffer[location_start_high_byte];
									 data_array[1] = RX_buffer[location_start_low_byte];
									 data_array[2] = RX_buffer[location_end_high_byte];
									 data_array[3] = RX_buffer[location_end_low_byte];

									 MODBUS_send_task(FC_write_multiple_coils, 4 , data_array);
								 }

								 else
								 {
									 error_flag = 1;
								 }
							  }

							  else
							  {
								  error_flag = 1;
							  }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_write_multiple_coils | 0x80), 1, data_array);
						   break;
						 }

						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 case FC_write_multiple_registers:
				 {
//					 temp1 = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);
					 uint16 RegsStartAddress = make_word(RX_buffer[location_start_high_byte], RX_buffer[location_start_low_byte]);

					 if((RegsStartAddress >= addr_holding_reg_start) && (RegsStartAddress <= addr_holding_reg_end))
					 {
//						 temp2 = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);
						 uint16 RegsDataLength = make_word(RX_buffer[location_end_high_byte], RX_buffer[location_end_low_byte]);
//						 temp3 = (temp2 + RegsStartAddress - 1);
						 uint16 RegsEndAddress = (RegsDataLength + RegsStartAddress - 1);

						 if((RegsDataLength <= no_of_holding_regs) && (RegsEndAddress <= addr_holding_reg_end))
						 {
//							 temp3 = RX_buffer[byte_size_byte];
							 uint16 ReciveDataLength = RX_buffer[byte_size_byte];
//							 temp4 = (RegsDataLength << 1);
							 uint16 CountingDataLength = RegsDataLength * 2;

							 if(ReciveDataLength == CountingDataLength)
							 {
//								 temp7 = (mandatory_bytes_to_read + ReciveDataLength);
								 uint16 StartCRCAddress = (mandatory_bytes_to_read + ReciveDataLength);
//								 temp5 = make_word(RX_buffer[(1 + LastDataByteAddress)], RX_buffer[LastDataByteAddress]);
								 uint16 RecivedCRC =  make_word(RX_buffer[(1 + StartCRCAddress)], RX_buffer[StartCRCAddress]);
//								 temp6 = MODBUS_RTU_CRC16(RX_buffer, LastDataByteAddress);
								 uint16 CountingCRC = MODBUS_RTU_CRC16(RX_buffer, StartCRCAddress);

								 if(CountingCRC == RecivedCRC)
								 {
//									 temp5 = (RegsStartAddress - addr_holding_reg_start);
									 uint16 RealHoldingsStartAddress = (RegsStartAddress - addr_holding_reg_start);
//									 temp3 = mandatory_bytes_to_read;
									 uint16 StartDataByteInReciveData = mandatory_bytes_to_read;

									 if(RegsDataLength == 1)
									 {
										 holding_registers[RealHoldingsStartAddress] = make_word(RX_buffer[mandatory_bytes_to_read], RX_buffer[(1 + mandatory_bytes_to_read)]);
									 }

									 else
									 {
										 for(uint16 counter = RealHoldingsStartAddress; counter < (RegsDataLength + RealHoldingsStartAddress); counter++)
										 {
											 holding_registers[counter] = make_word(RX_buffer[StartDataByteInReciveData], RX_buffer[(1 + StartDataByteInReciveData)]);
											 StartDataByteInReciveData += 2;
										 }
									 }

									 data_array[0] = RX_buffer[location_start_high_byte];
									 data_array[1] = RX_buffer[location_start_low_byte];
									 data_array[2] = RX_buffer[location_end_high_byte];
									 data_array[3] = RX_buffer[location_end_low_byte];

									 MODBUS_send_task(FC_write_multiple_registers, 4 , data_array);
								 }

								 else
								 {
									 error_flag = 1;
								 }
							  }

							  else
							  {
								  error_flag = 1;
							  }
						 }

						 else
						 {
							 error_flag = 1;
						 }
					 }

					 else
					 {
						 error_flag = 1;
					 }

					 switch(error_flag)
					 {
						 case 1:
						 {
						   data_array[0] = 0x02;
						   MODBUS_send_task((FC_write_multiple_registers | 0x80), 1, data_array);
						   break;
						 }

						 default:
						 {
						   break;
						 }
					 }

					 break;
				 }

				 default:
				 {
					 break;
				 }
			 }

		 }
		 flush_RX_buffer();

     }


}


void MODBUS_send_task(uint8 function_code, uint8 data_length, uint8 *values)
{
     uint8 hb = 0x00;
     uint8 lb = 0x00;
     uint8 byte_count = 0x00;

     flush_TX_buffer();

     TX_buffer[id_byte] = own_ID;
     TX_buffer[function_code_byte] = function_code;

     for(byte_count = 0; byte_count < data_length; byte_count++)
     {
         TX_buffer[2 + byte_count] = values[byte_count];
     }

     get_HB_LB(MODBUS_RTU_CRC16(TX_buffer, (data_length + 2)), &hb, &lb);

     TX_buffer[2 + data_length] = lb;
     TX_buffer[3 + data_length] = hb;

     TransmitENA();

     HAL_UART_Transmit(&huart3, TX_buffer, (4 + data_length), 10);
//     HAL_UART_Transmit_IT(&huart3, TX_buffer, (4 + data_length));

     /*for(byte_count = 0; byte_count < (6 + data_length); byte_count++)
     {
//         UART1_Write(TX_buffer[byte_count]);
         HAL_UART_Transmit(&huart3, (TX_buffer + byte_count), 1, 10);
     }*/

     MODBUS_start();
}


void ReciveENA() {
	HAL_GPIO_WritePin(RS485nRE_GPIO_Port, RS485nRE_Pin, GPIO_PIN_RESET);
}

void TransmitENA() {
	HAL_GPIO_WritePin(RS485nRE_GPIO_Port, RS485nRE_Pin, GPIO_PIN_SET);
}

uint16 make_word(uint8 HB, uint8 LB)
{
    uint16 tmp = 0;

    tmp = HB;
    tmp <<= 8;
    tmp |= LB;

    return tmp;
}


void get_HB_LB(uint16 value, uint8 *HB, uint8 *LB)
{
    *LB = (value & 0x00FF);
    *HB = ((value & 0xFF00) >> 0x08);
}
