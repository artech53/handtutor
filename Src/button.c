/*
 * button.c
 *
 *  Created on: 22 окт. 2019 г.
 *      Author: max_i
 */
#include "main.h"

buttonstate
	ButtonState = idle,
	ButtonNewState = idle;

Motor * motor[5];

void ButtonsInit( Motor * motor1, Motor * motor2, Motor * motor3, Motor * motor4, Motor * motor5) {
	motor[0] = motor1;
	motor[1] = motor2;
	motor[2] = motor3;
	motor[3] = motor4;
	motor[4] = motor5;
}


uint8 ButtonsCheck(uint16 corner[5]) {
	GPIO_PinState button1 = HAL_GPIO_ReadPin(Button1_GPIO_Port, Button1_Pin);
	GPIO_PinState button2 = HAL_GPIO_ReadPin(Button2_GPIO_Port, Button2_Pin);
	GPIO_PinState button3 = HAL_GPIO_ReadPin(Button3_GPIO_Port, Button3_Pin);

	switch(ButtonState) {

		case reset:
		case idle:
		case button1_pressed:
		case button2_pressed: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(button3 == GPIO_PIN_RESET) {
				ButtonState = button3_press;
			} else {
				ButtonState = idle;
			}
		} break;

		case button3_press: {
			ButtonState = button3_finger1_compress;
		} break;

		case button3_finger1_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[0] > motor[0]->Coner.Min + 10) {
				ButtonState = button3_finger1_compress;
			} else  {
				ButtonState = button3_finger2_compress;
			}
		} break;

		case button3_finger2_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[1]> motor[1]->Coner.Min + 10) {
				ButtonState = button3_finger2_compress;
			} else  {
				ButtonState = button3_finger3_compress;
			}
		} break;

		case button3_finger3_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[2]> motor[2]->Coner.Min + 10) {
				ButtonState = button3_finger3_compress;
			} else  {
				ButtonState = button3_finger4_compress;
			}
		} break;

		case button3_finger4_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[3]> motor[3]->Coner.Min + 10) {
				ButtonState = button3_finger4_compress;
			} else  {
				ButtonState = button3_finger4_de_compress;//button3_pastie_compress;
			}
		} break;

		case button3_pastie_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[4]> motor[4]->Coner.Min + 10) {
				ButtonState = button3_pastie_compress;
			} else  {
				ButtonState = button3_pastie_de_compress;
			}
		} break;

		case button3_pastie_de_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[4]< motor[4]->Coner.Max - 10) {
				ButtonState = button3_pastie_compress;
			} else  {
				ButtonState = button3_finger4_de_compress;
			}
		} break;

		case button3_finger4_de_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[3]< motor[3]->Coner.Max - 10) {
				ButtonState = button3_finger4_de_compress;
			} else  {
				ButtonState = button3_finger3_de_compress;
			}
		} break;

		case button3_finger3_de_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[2]< motor[2]->Coner.Max - 10) {
				ButtonState = button3_finger3_de_compress;
			} else  {
				ButtonState = button3_finger2_de_compress;
			}
		} break;

		case button3_finger2_de_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[1]< motor[1]->Coner.Max - 10) {
				ButtonState = button3_finger2_de_compress;
			} else  {
				ButtonState = button3_finger1_de_compress;
			}
		} break;

		case button3_finger1_de_compress: {
			if(button1 == GPIO_PIN_RESET) {
				ButtonState = button1_pressed;
			} else if(button2 == GPIO_PIN_RESET) {
				ButtonState = button2_pressed;
			} else if(corner[0]< motor[0]->Coner.Max - 10) {
				ButtonState = button3_finger1_de_compress;
			} else  {
				ButtonState = idle;
			}
		} break;


	}

	switch(ButtonState) {

		case reset: {

		} break;
		case idle: {

		} break;
		case button1_pressed: {

			HandMove(motor, 30000, corner, 0);

			return 1;
		} break;
		case button2_pressed: {
			HandMove(motor, -30000, corner, 0);
			return 1;
		} break;

		case button3_finger1_compress: {
			HandMove(motor, -20000, corner, 1);
			return 1;
		} break;

		case button3_finger2_compress: {
			HandMove(motor, -20000, corner, 2);
			return 1;
		} break;

		case button3_finger3_compress: {
			HandMove(motor, -20000, corner, 3);
			return 1;
		} break;

		case button3_finger4_compress: {
			HandMove(motor, -20000, corner, 4);
			return 1;
		} break;

		case button3_pastie_compress: {
			HandMove(motor, -20000, corner, 5);
			return 1;
		} break;

		case button3_pastie_de_compress: {
			HandMove(motor, 20000, corner, 5);
			return 1;
		} break;

		case button3_finger4_de_compress: {
			HandMove(motor, 20000, corner, 4);
			return 1;
		} break;

		case button3_finger3_de_compress: {
			HandMove(motor, 20000, corner, 3);
			return 1;
		} break;

		case button3_finger2_de_compress: {
			HandMove(motor, 20000, corner, 2);
			return 1;
		} break;

		case button3_finger1_de_compress: {
			HandMove(motor, 20000, corner, 1);
			return 1;
		} break;


	}

	return 0;
}

void HandMove(Motor * motor[], int16 Power, uint16 Corner[], uint8 motorNumber) {

	if(motorNumber > 0) {
		for(uint8 i = 0; i < 5; i++) {
			if(i != (motorNumber - 1)) Motor_Move(motor[i], 0, Corner[i]);
		}

		Motor_Move(motor[motorNumber - 1], Power, Corner[motorNumber - 1]);
	} else {
		for(uint8 i = 0; i < 5; i++) {
			Motor_Move(motor[i], Power, Corner[i]);
		}
	}
}

