/*
 * Encoders.c
 *
 *  Created on: Jul 21, 2019
 *      Author: maxim
 */

#include "Encoders.h"

#define ENCODERS_MAX 5

#define FILTER 5

typedef struct  {
	uint16 Increments;
	uint16 Freq;
}encoder_type;

// incremental encoders
encoder_type encoders_inc[ENCODERS_MAX];



// analog_absolute encoders
uint32 encoders_abs[ENCODERS_MAX + 2];// + 8];
uint32 encoders_abs_filter[ENCODERS_MAX + 2][FILTER];


uint8 EncodersGetCount() {
	return ENCODERS_MAX;
}

// WORK WITH INCREMENTAL ENCODERS
int8 Encoder_SetIncrement(ENCODER_NUMBER encoder_number, uint16 value) {

	encoders_inc[encoder_number].Increments = value;

	// all OK
	return 1;
}

uint16 Encoder_GetIncrement(ENCODER_NUMBER encoder_number) {

	return encoders_inc[encoder_number].Increments;
}

int8 Encoder_SetFreq(ENCODER_NUMBER encoder_number, uint16 value) {

	encoders_inc[encoder_number].Freq = value;

	// all OK
	return 1;
}
uint16 Encoder_GetFreq(ENCODER_NUMBER encoder_number) {

	return encoders_inc[encoder_number].Freq;
}

int8 Encoder_Increment(ENCODER_NUMBER encoder_number) {

	encoders_inc[encoder_number].Increments = (encoders_inc[encoder_number].Increments + 1);
}

void Encoders_Reset() {

	for(uint8 i = 0; i < ENCODERS_MAX; i++) {
		encoders_inc[i].Freq = 0;
		encoders_inc[i].Increments = 0;
	}
}

//========================================

// WORK WITH ABSOLUTE ENCODERS
int8 canRead = -1;

int8 StartReadEncoders() {
	canRead = -1;
}

int8 StopReadEncoders() {
	canRead = 1;
}

int8 isCanReadEncoders() {
	return canRead;
}

// добавляем итерацию фильтрации данных после чтения оных
void EncoderAbs_Filter(){

	for(uint8 encoder_number = 0; encoder_number < ENCODERS_MAX + 2; encoder_number++) {
		for(uint8 i = 0; i < FILTER - 1; i++) {
			encoders_abs_filter[encoder_number][i] = encoders_abs_filter[encoder_number][i + 1];
		}
		encoders_abs_filter[encoder_number][FILTER - 1] = encoders_abs[encoder_number];
	}
}

int8 EncoderAbs_Set(ENCODER_NUMBER encoder_number, uint16 value) {
	encoders_abs[encoder_number]= value;

	// all OK
	return 1;
}

uint16 EncoderAbs_Get(ENCODER_NUMBER encoder_number) {

	uint32 encoderFilter = 0;

	for(uint8 i = 0; i < FILTER; i++) {

		encoderFilter += encoders_abs_filter[encoder_number][i];
	}

	return encoderFilter / FILTER;
}

void EncodersAbs_Reset() {

	for(uint8 i = 0; i < ENCODERS_MAX + 2/* + 8*/; i++) {
		encoders_abs[i] = 0;
	}

	for(uint8 encoder_number = 0; encoder_number < ENCODERS_MAX + 2; encoder_number++) {
		for(uint8 i = 0; i < FILTER; i++) {
			encoders_abs_filter[encoder_number][i] = 0;
		}
	}
}

uint32* EncodersAbs_GetHandle() {
	return encoders_abs;
}


//========================================
