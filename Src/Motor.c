/*
 * Motor.cpp
 *
 *  Created on: Jul 24, 2019
 *      Author: maxim
 *
 *      текущая верия поддерживает работу ТОЛЬКО С ПЕРВЫМ ТАЙМЕРОМ (TIM1)
 */

#include "main.h"
#include "Motor.h"

#define CORNER_MAX	370 /* 400 */
#define CORNER_MIN	170 /* 240 */

int8 isCanDirectionToMax(uint16 corner_max, uint16 corner, int32 power) ;
int8 isCanDirectionToMin(uint16 corner_min, uint16 corner, int32 power) ;

void PWMConfig(Motor * motor, int32 power) {
	HAL_TIM_PWM_Stop(motor->PWM_Config.htim, motor->PWM_Config.Channel);

	power = 65535 - (uint32) power;

	motor->PWM_Config.sConfigOC.Pulse = (uint16)((power) & 0xFFFF);
	if (HAL_TIM_PWM_ConfigChannel(motor->PWM_Config.htim, &motor->PWM_Config.sConfigOC, motor->PWM_Config.Channel) != HAL_OK)
	{
		Error_Handler();
	}

	HAL_TIM_PWM_Start(motor->PWM_Config.htim, motor->PWM_Config.Channel);
}

void Motor_Init(Motor* motor, TIM_HandleTypeDef *htim, uint32_t Channel, uint8 timnumber)
{
	motor->direction = 0;
	motor->GPIO_direction = 0;
	motor->isAtteched = 0;
	motor->isError = 0;
	motor->PWM_Config.TimNumber = timnumber;
	motor->lastPower = 0;

	motor->Coner.Max = CORNER_MAX;
	motor->Coner.Min = CORNER_MIN;
/*
	motor->PWM_Config.sBreakDeadTimeConfig = {0, 0, 0, 0, 0, 0, 0, 0};
	motor->PWM_Config.sClockSourceConfig	= {};
	motor->PWM_Config.sConfigOC				= {};
	motor->PWM_Config.sMasterConfig			= {};
*/
	motor->PWM_Config.htim = htim;
	motor->PWM_Config.Channel = Channel;
}

void Motor_Attach(Motor* motor, GPIO_TypeDef* GPIO_direction, uint16_t direction) {
	if (!motor->isAtteched) // If motor A is NOT attached...
	{

		// ...attach motor A to the input pins.
		motor->direction = direction;
		motor->GPIO_direction = GPIO_direction;

		// Show the motor is attached.
		motor->isAtteched = 1;

		// Initialize as LOW.
		HAL_GPIO_WritePin(motor->GPIO_direction, motor->direction, GPIO_PIN_RESET);

		HAL_TIM_PWM_Stop(motor->PWM_Config.htim, motor->PWM_Config.Channel);
	}
}

// motor - Motor* структура
// power - словная мощность -32768 .. 0 .. +32767
void Motor_Move(Motor * motor, int32 power, uint16 corner) {
	int8 max = -1;
	int8 min = -1;
//	uint16 freq = Encoder_GetFreq(ENCODER_1);

	if((motor->PWM_Config.TimNumber == 4)) {

		if(power == 0) power = 1;

		max = isCanDirectionToMax(CORNER_MAX, corner, power);
		min = isCanDirectionToMin(CORNER_MIN + 80, corner, power);
	} else {
		max = isCanDirectionToMax(CORNER_MAX, corner, power);
		min = isCanDirectionToMin(CORNER_MIN, corner, power);
	}

	if(motor->lastPower != power) {
	// если новая мощьность отличается - то стараемся запустить мотор
	// сбрасываем ошибку привода
		motor->lastPower = power;
		motor->isError = 0;

		power *= 2;

		if(power > 0) {
			if(max > 0) MotorForward(motor, power);
			else MotorStop(motor); // */
		} else if(power < 0) {
			if(min > 0) MotorBackward(motor, power);
			else MotorStop(motor); // */
		} else {
			MotorStop(motor);
		}
	} else {
		if(motor->isError > 1) {
			MotorStop(motor);
		} else {
			if(power > 0) {
				if(max <= 0)
					MotorStop(motor); // */
			} else if(power < 0) {
				if(min <= 0)
					MotorStop(motor);
			} else {
				MotorStop(motor);
			}
		}
	}
}

void MotorBackward(Motor * motor, int32 power) {

	if(power > 0) return;

	HAL_GPIO_WritePin(motor->GPIO_direction, motor->direction, GPIO_PIN_RESET);

	PWMConfig(motor, power);
}

void MotorForward(Motor * motor, int32 power) {

	if(power < 0) return;

	HAL_GPIO_WritePin(motor->GPIO_direction, motor->direction, GPIO_PIN_SET);

	PWMConfig(motor, power);
}

void MotorStop(Motor * motor) {
	HAL_TIM_PWM_Stop(motor->PWM_Config.htim, motor->PWM_Config.Channel);
	HAL_GPIO_WritePin(motor->GPIO_direction, motor->direction, GPIO_PIN_RESET);
}

int8 isCanDirectionToMax(uint16 corner_max, uint16 corner, int32 power) {
// максимальное значение при положительной мощности

	if(power < 0) return 1; // если мощность меньше нуля - нам пофигу начение угла максимального

	if(corner >= corner_max) return 0;

	return 1;
}

int8 isCanDirectionToMin(uint16 corner_min, uint16 corner, int32 power) {
// минимальное когда мы двигаемся при отрицательной мощности

	if(power > 0) return 1;

	if(corner <= corner_min) return 0;

	return 1;
}


