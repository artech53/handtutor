/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/*
 * ОЧЕНЬ ВАЖНО ПР�? РАБОТЕ УЧЕСТЬ, ЧТО параметры тайеров формируются автоматом, а значи их надо читать в тех методах, в которых они формируются
 * это не удобно с т.з. архитектуры. но зато автоматическая енерация и прочие прелести
 * для КАЖДОГО КАНАЛА МОТОРА надо сохранять состояние инициализированного таймера в методе его ининциализации
 *
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define SYSCLK 60000000

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */

uint8 own_ID = 0x01;
// указывает скорость по умолчанию
uint32 bauderate = 115200;

// Пять моторов - 5 структур работы с моторами
Motor motor1 = {0};
Motor motor2 = {0};
Motor motor3 = {0};
Motor motor4 = {0};
Motor motor5 = {0};



// нужна исключительно для старта таймера определения окончания послки по modbus
uint32 watchdog = 0;


// объединение для работы с HOLDINGS регистрами и параметрами системы
union {
	uint16 Regs[21];				// REG

	struct {
		uint16 Set_I_MotorPower;	// 1
		uint16 Set_II_MotorPower;	// 2
		uint16 Set_III_MotorPower;	// 3
		uint16 Set_IV_MotorPower;	// 4
		uint16 Set_V_MotorPower;	// 5

		uint16 Get_I_Corner;		// 6
		uint16 Get_I_Freq;			// 7
		uint16 Get_II_Corner;		// 8
		uint16 Get_II_Freq;			// 9
		uint16 Get_III_Corner;		// 10
		uint16 Get_III_Freq;		// 11
		uint16 Get_IV_Corner;		// 12
		uint16 Get_IV_Freq;			// 13
		uint16 Get_V_Corner;		// 14
		uint16 Get_V_Freq;			// 15

		uint16 Get_Current;			// 16
		uint16 Get_Votage;			// 17

		uint16 Get_own_ID;			// 18
		uint16 Get_baude;			// 19

		uint16 Set_own_ID;			// 20
		uint16 Set_baude;			// 21
	}Reg;

}HoldingRegs;




/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM7_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM5_Init(void);
/* USER CODE BEGIN PFP */
void UART_BaudeChange(uint32 baude);
uint8 BauderateCheck(uint32 baude);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
float VoltageGet() {

	float vlc = (float)EncoderAbs_Get(6);
	float coef = (3.3f / 4096.0f); // умножаем на 2 т.к. входной делитель делит напржение на 2:)

	float voltage = vlc * coef * 2.83f;
	return voltage;
}


float CurrentGet() {
	// наш датчик имеет 5А
	// 185 мВ/А
	// 0А = 3,3. / 2

	float crt = (float)EncoderAbs_Get(5); // взяли данные с датчика
	float coef = (3.3f / 4096.0f);	//коэффициент АЦП (вольт / разряд
	float zero = 5.0f / 2.0f; // среднее значение напряжения эквивалентное нулевому току (питаниелатика от 5 В)

	float votage = crt * coef - zero; // определили номинал напряжения, определённого на АЦП и преенсим его на нулевую точку датчика тока

	float current = votage / 0.185f;

	return current;
}
//Отработка прерывания перепонения таймеров
uint8 LED_Stage = 0;
uint32 counter = 0;
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	if(htim == &htim7) {
	// прерывание таймера контроля передачи по modbus
		if(!FrameGet()) {
			FrameSet(1);
		}else {
			FrameSet(0);
		}

	} else if(htim == &htim6) {
		// считаем частоту вращения моторов
		// текущие настройки приводят к режиму 8Гц

		for(int i = 0; i < EncodersGetCount(); i++) {
			uint16 freq = SYSCLK / htim6.Instance->PSC / htim6.Instance->ARR; // определяем частоту работы таймера
			// freq раз в секунду мы обнуляем счетчик импульсов
			// значит надо полученное число импульсов умножить на freq и получим импульсов в секунду
			// каждые 2 импульса - один оборот

			// разделим общее число импульсов на 2 и получим число оборотов в секунду
			Encoder_SetFreq(i, (Encoder_GetIncrement(i) * freq) / 3);// / (2000 / (htim6.Instance->ARR)));
			Encoder_SetIncrement(i, 0);
		}

		if(Encoder_GetFreq(ENCODER_1) == 0 && abs(motor1.lastPower) > 1) motor1.isError += 1;
		if(Encoder_GetFreq(ENCODER_2) == 0 && abs(motor2.lastPower) > 1) motor2.isError += 1;
		if(Encoder_GetFreq(ENCODER_3) == 0 && abs(motor3.lastPower) > 1) motor3.isError += 1;
		if(Encoder_GetFreq(ENCODER_4) == 0 && abs(motor4.lastPower) > 1) motor4.isError += 1;
		if(Encoder_GetFreq(ENCODER_5) == 0 && abs(motor5.lastPower) > 1) motor5.isError += 1;

		if(!LED_Stage) {
			LED_Stage = 1;
			HAL_GPIO_WritePin(D3_GPIO_Port, D3_Pin, GPIO_PIN_RESET);
		}else {
			LED_Stage = 0;
			HAL_GPIO_WritePin(D3_GPIO_Port, D3_Pin, GPIO_PIN_SET);
		}

		counter++;
	}
}

// отработка прерывания по энкодерам инкрементным
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	switch(GPIO_Pin) {

	case Enc1_Pin: {
		Encoder_Increment(0);
		motor1.isError = 0;

		}break;
	case Enc2_Pin: {
		Encoder_Increment(1);
		motor2.isError = 0;

		}break;
	case Enc3_Pin: {
		Encoder_Increment(2);
		motor3.isError = 0;

		}break;
	case Enc4_Pin: {
		Encoder_Increment(3);
		motor4.isError = 0;

		}break;
	case Enc5_Pin: {
		Encoder_Increment(4);
		motor5.isError = 0;

		}break;
	}
}

void CornerUpdate() {
	// читаем состояние системы и пишем в HOLDINGS буфер
	  HoldingRegs.Reg.Get_I_Corner 		= EncoderAbs_Get(ENCODER_2) / 10;
	  HoldingRegs.Reg.Get_I_Freq		= Encoder_GetFreq(ENCODER_1) / 10;

	  HoldingRegs.Reg.Get_II_Corner		= EncoderAbs_Get(ENCODER_3) / 10;
	  HoldingRegs.Reg.Get_II_Freq		= Encoder_GetFreq(ENCODER_2) / 10;

	  HoldingRegs.Reg.Get_III_Corner	= EncoderAbs_Get(ENCODER_4) / 10;
	  HoldingRegs.Reg.Get_III_Freq		= Encoder_GetFreq(ENCODER_3) / 10;

	  HoldingRegs.Reg.Get_IV_Corner 	= EncoderAbs_Get(ENCODER_5) / 10;
	  HoldingRegs.Reg.Get_IV_Freq		= Encoder_GetFreq(ENCODER_4) / 10;

	  HoldingRegs.Reg.Get_V_Corner 		= EncoderAbs_Get(ENCODER_1) / 10;
	  HoldingRegs.Reg.Get_V_Freq		= Encoder_GetFreq(ENCODER_5) / 10;

	  // надо переделать пересчет значений во что-то человекоподобное
	  HoldingRegs.Reg.Get_Current		= (uint16)(CurrentGet() * 100);
	  HoldingRegs.Reg.Get_Votage		= (uint16)(VoltageGet() * 100);

	  HoldingRegs.Reg.Get_own_ID		= own_ID;
	  HoldingRegs.Reg.Get_baude			= bauderate / 100;
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM3_Init();
  MX_USART3_UART_Init();
  MX_TIM7_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_TIM5_Init();
  /* USER CODE BEGIN 2 */

  // срасываем значения энкодеров в состояние по умолчанию
  Encoders_Reset();
  EncodersAbs_Reset();

  // подключаем моторы к пинам
	Motor_Init(&motor1, &htim3, TIM_CHANNEL_1, 3);
	Motor_Attach(&motor1, M1_GPIO_Port, M1_Pin);

	Motor_Init(&motor2, &htim3, TIM_CHANNEL_2, 3);
	Motor_Attach(&motor2, M2_GPIO_Port, M2_Pin);

	Motor_Init(&motor3, &htim3, TIM_CHANNEL_3, 3);
	Motor_Attach(&motor3, M3_GPIO_Port, M3_Pin);

	Motor_Init(&motor4, &htim3, TIM_CHANNEL_4, 3);
	Motor_Attach(&motor4, M4_GPIO_Port, M4_Pin);

	Motor_Init(&motor5, &htim4, TIM_CHANNEL_2, 4);
	Motor_Attach(&motor5, M5_GPIO_Port, M5_Pin);


	ButtonsInit(&motor1, &motor2, &motor3, &motor4, &motor5);

	// инициализируем таймер контроля обмена modbus
  HAL_TIM_Base_Start_IT(&htim7);

  while(!isNormalWork() && watchdog < 100000){
	  watchdog++;
  };

  // запускаем подсчет частоты вращения мотора
  HAL_TIM_Base_Start_IT(&htim6);

  HoldingsRegistersInit(own_ID);

  // рбааем приём
  HAL_GPIO_WritePin(RS485nRE_GPIO_Port, RS485nRE_Pin, GPIO_PIN_SET);

  // инициализируем и запускаем ообмен по модбас
  MODBUS_start();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {



//	  continue;
	  // запускаем чтение аналоговых портов
	  // делаем это через DMA, чтобы не грузить ядро
	  // тут же читаем значения Напряжения и Тока ВН�?МАТЕЛЬНЕЕ
	  HAL_ADC_Start_DMA(&hadc1, EncodersAbs_GetHandle(), EncodersGetCount() + 2);// + 8) ;

	  EncoderAbs_Filter();
	  CornerUpdate();

	  uint16 corners[] = {
		  	  	  	  	  HoldingRegs.Reg.Get_I_Corner,
		  	  	  	  	  HoldingRegs.Reg.Get_II_Corner,
						  HoldingRegs.Reg.Get_III_Corner,
						  HoldingRegs.Reg.Get_IV_Corner,
						  HoldingRegs.Reg.Get_V_Corner
	  	  	  	  	  	 };
	  if(ButtonsCheck(corners)) continue;

	  // подготовим holding regs для отправки мастеру
	  RegsCopy((uint16*)HoldingRegs.Regs, GetHoldingRegisters(), 6, 19);

	  // отрабатываем протокол модбас, если там есть, что отрабатывать
	  MODBUS_receive_task();

	  // приняли от мастера данные - надо отработать
	  RegsCopy(GetHoldingRegisters(), (uint16*)HoldingRegs.Regs, 1, 5);
	  RegsCopy(GetHoldingRegisters(), (uint16*)HoldingRegs.Regs, 20, 21);

	  // отработка принятой команды по модбас
	  Motor_Move(&motor1, (int16)HoldingRegs.Reg.Set_I_MotorPower, HoldingRegs.Reg.Get_I_Corner);
	  Motor_Move(&motor2, (int16)HoldingRegs.Reg.Set_II_MotorPower, HoldingRegs.Reg.Get_II_Corner);
	  Motor_Move(&motor3, (int16)HoldingRegs.Reg.Set_III_MotorPower, HoldingRegs.Reg.Get_III_Corner);
	  Motor_Move(&motor4, (int16)HoldingRegs.Reg.Set_IV_MotorPower, HoldingRegs.Reg.Get_IV_Corner);
	  Motor_Move(&motor5, (int16)HoldingRegs.Reg.Set_V_MotorPower, HoldingRegs.Reg.Get_V_Corner);

	  // тут будет отработка сменя скорости обмена и идентификатора изделия
	  own_ID = HoldingRegs.Reg.Set_own_ID;

	  UART_BaudeChange(HoldingRegs.Reg.Set_baude * 100);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 7;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = 3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 7;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 60000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 10;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */
//  motor1.PWM_Config.sBreakDeadTimeConfig = sBreakDeadTimeConfig;
	motor1.PWM_Config.sClockSourceConfig = sClockSourceConfig;
	motor1.PWM_Config.sConfigOC = sConfigOC;
	motor1.PWM_Config.sMasterConfig = sMasterConfig;

	motor2.PWM_Config.sClockSourceConfig = sClockSourceConfig;
	motor2.PWM_Config.sConfigOC = sConfigOC;
	motor2.PWM_Config.sMasterConfig = sMasterConfig;

	motor3.PWM_Config.sClockSourceConfig = sClockSourceConfig;
	motor3.PWM_Config.sConfigOC = sConfigOC;
	motor3.PWM_Config.sMasterConfig = sMasterConfig;

	motor4.PWM_Config.sClockSourceConfig = sClockSourceConfig;
	motor4.PWM_Config.sConfigOC = sConfigOC;
	motor4.PWM_Config.sMasterConfig = sMasterConfig;
  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 60000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 10;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

	motor5.PWM_Config.sClockSourceConfig = sClockSourceConfig;
	motor5.PWM_Config.sConfigOC = sConfigOC;
	motor5.PWM_Config.sMasterConfig = sMasterConfig;

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 0;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 0;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 60000;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 125;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 521;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 35;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OnePulse_Init(&htim7, TIM_OPMODE_SINGLE) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_2;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  bauderate = huart3.Init.BaudRate;
  /* USER CODE END USART3_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, M1_Pin|M2_Pin|M3_Pin|M4_Pin 
                          |D3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RS485nRE_Pin|M5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : M1_Pin M2_Pin M3_Pin M4_Pin 
                           D3_Pin */
  GPIO_InitStruct.Pin = M1_Pin|M2_Pin|M3_Pin|M4_Pin 
                          |D3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : Button2_Pin Button3_Pin Button1_Pin */
  GPIO_InitStruct.Pin = Button2_Pin|Button3_Pin|Button1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : RS485nRE_Pin M5_Pin */
  GPIO_InitStruct.Pin = RS485nRE_Pin|M5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : Enc1_Pin Enc3_Pin */
  GPIO_InitStruct.Pin = Enc1_Pin|Enc3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : Enc2_Pin Enc4_Pin Enc5_Pin */
  GPIO_InitStruct.Pin = Enc2_Pin|Enc4_Pin|Enc5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 4 */

// Копируеум регистры из одного массива в другой
void RegsCopy(uint16* FromArray, uint16* ToArray, uint8 startReg, uint8 endReg) {
	for(uint8 i = (startReg - 1); i <= (endReg - 1); i++) {
		ToArray[i] = FromArray[i];
	}
}

void UART_BaudeChange(uint32 baude) {

	if((baude != bauderate) && BauderateCheck(baude)) {

		bauderate = baude;
		huart3.Init.BaudRate = bauderate;
		if (HAL_UART_Init(&huart3) != HAL_OK)
		{
			Error_Handler();
		}

		// рбааем приём
		HAL_GPIO_WritePin(RS485nRE_GPIO_Port, RS485nRE_Pin, GPIO_PIN_SET);

		// инициализируем и запускаем ообмен по модбас
		MODBUS_start();
	}
}

uint8 BauderateCheck(uint32 baude) {

	switch(baude) {
	default: return 0;

	case 4800: {
				return 1;
			}break;

	case 14400: {
				return 1;
			}break;

	case 19200: {
				return 1;
			}break;

	case 28800: {
				return 1;
			}break;
	case 38400: {
				return 1;
			}break;
	case 57600: {
				return 1;
			}break;
	case 115200: {
				return 1;
			}break;
	case 128000: {
				return 1;
			}break;
	case 256000: {
				return 1;
			}break;
	case 921600: {
				return 1;
			}break;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
