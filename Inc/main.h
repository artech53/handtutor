/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "types.h"
#include "globals.h"
#include "Encoders.h"
#include "Motor.h"

#include "modbus.h"

#include "button.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

void RegsCopy(uint16* FromArray, uint16* ToArray, uint8 startReg, uint8 endReg);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define M1_Pin GPIO_PIN_0
#define M1_GPIO_Port GPIOC
#define M2_Pin GPIO_PIN_1
#define M2_GPIO_Port GPIOC
#define M3_Pin GPIO_PIN_2
#define M3_GPIO_Port GPIOC
#define M4_Pin GPIO_PIN_3
#define M4_GPIO_Port GPIOC
#define Current_Pin GPIO_PIN_5
#define Current_GPIO_Port GPIOA
#define Voltage_Pin GPIO_PIN_6
#define Voltage_GPIO_Port GPIOA
#define M2P_Pin GPIO_PIN_7
#define M2P_GPIO_Port GPIOA
#define D3_Pin GPIO_PIN_5
#define D3_GPIO_Port GPIOC
#define M3P_Pin GPIO_PIN_0
#define M3P_GPIO_Port GPIOB
#define M4P_Pin GPIO_PIN_1
#define M4P_GPIO_Port GPIOB
#define Button2_Pin GPIO_PIN_12
#define Button2_GPIO_Port GPIOB
#define RS485nRE_Pin GPIO_PIN_13
#define RS485nRE_GPIO_Port GPIOB
#define Button3_Pin GPIO_PIN_14
#define Button3_GPIO_Port GPIOB
#define Button1_Pin GPIO_PIN_15
#define Button1_GPIO_Port GPIOB
#define Enc1_Pin GPIO_PIN_6
#define Enc1_GPIO_Port GPIOC
#define Enc1_EXTI_IRQn EXTI9_5_IRQn
#define Enc3_Pin GPIO_PIN_7
#define Enc3_GPIO_Port GPIOC
#define Enc3_EXTI_IRQn EXTI9_5_IRQn
#define M1P_Pin GPIO_PIN_4
#define M1P_GPIO_Port GPIOB
#define Enc2_Pin GPIO_PIN_5
#define Enc2_GPIO_Port GPIOB
#define Enc2_EXTI_IRQn EXTI9_5_IRQn
#define M5_Pin GPIO_PIN_6
#define M5_GPIO_Port GPIOB
#define M5P_Pin GPIO_PIN_7
#define M5P_GPIO_Port GPIOB
#define Enc4_Pin GPIO_PIN_8
#define Enc4_GPIO_Port GPIOB
#define Enc4_EXTI_IRQn EXTI9_5_IRQn
#define Enc5_Pin GPIO_PIN_9
#define Enc5_GPIO_Port GPIOB
#define Enc5_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
