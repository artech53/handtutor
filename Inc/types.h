/*
 * types.h
 *
 *  Created on: Jul 21, 2019
 *      Author: maxim
 */

#ifndef TYPES_H_
#define TYPES_H_

#define	ON	1
#define OFF	0

// преобразуем типы в боее понятные имена
// настоящий макрос упрощает запись объявления нового типа
// достаточно указать только беззнаковые имена типов - макрос автоматом создаст эквивалент для знакового типа
#define typeinit(from, to)	typedef from to; \
							typedef unsigned from u##to

typeinit(char, int8);
typeinit(short, int16);
typeinit(long int, int32);
typeinit(long long, int64);

typedef _Bool bool;

typedef struct {

	uint16 Encoder_1;
	uint16 Encoder_2;
	uint16 Encoder_3;
	uint16 Encoder_4;
	uint16 Encoder_5;
}Encoders_type;

typedef enum{

	ENCODER_1 = 0,
	ENCODER_2 = 1,
	ENCODER_3 = 2,
	ENCODER_4 = 3,
	ENCODER_5 = 4

}ENCODER_NUMBER;




#endif /* TYPES_H_ */
