/*
 * Motor.h
 *
 *  Created on: Jul 24, 2019
 *      Author: maxim
 */

#ifndef MOTOR_H_
#define MOTOR_H_


typedef struct{
	uint16_t direction;
	GPIO_TypeDef* GPIO_direction;

	int16_t lastPower;

	struct {
		uint16 Max;
		uint16 Min;
	}Coner;

	struct{
		TIM_HandleTypeDef *htim;
		uint32_t Channel;
		uint8 TimNumber;

		TIM_ClockConfigTypeDef sClockSourceConfig;
		TIM_MasterConfigTypeDef sMasterConfig;
		TIM_OC_InitTypeDef sConfigOC;
		TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;
	}PWM_Config;

	bool isAtteched;

	uint8 isError;
}Motor;


void Motor_Init(Motor* motor, TIM_HandleTypeDef *htim, uint32_t Channel, uint8 timnumber);

void Motor_Attach(Motor * motor, GPIO_TypeDef* GPIO_direction, uint16_t direction);
void Motor_Move(Motor * motor, int32 power, uint16 corner);

void MotorStop(Motor * motor);

void MotorForward(Motor * motor, int32 power);
void MotorBackward(Motor * motor, int32 power);




#endif /* MOTOR_H_ */
