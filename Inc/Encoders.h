/*
 * Encoders.h
 *
 *  Created on: Jul 21, 2019
 *      Author: maxim
 */

#ifndef ENCODERS_H_
#define ENCODERS_H_

#include "types.h"

uint8 EncodersGetCount();

// WORK WITH INCREMENTAL ENCODERS
int8 Encoder_SetIncrement(ENCODER_NUMBER, uint16);
uint16 Encoder_GetIncrement(ENCODER_NUMBER);
int8 Encoder_SetFreq(ENCODER_NUMBER, uint16);
uint16 Encoder_GetFreq(ENCODER_NUMBER);
int8 Encoder_Increment(ENCODER_NUMBER);

void Encoders_Reset();

//========================================

// WORK WITH ABSOLUTLE ENCODERS
int8 EncoderAbs_Set(ENCODER_NUMBER, uint16);

uint16 EncoderAbs_Get(ENCODER_NUMBER);

int8 EncoderAbs_Increment(ENCODER_NUMBER);

void EncodersAbs_Reset();

uint32* EncodersAbs_GetHandle();

int8 StartReadEncoders() ;
int8 StopReadEncoders();
int8 isCanReadEncoders();

void EncoderAbs_Filter();

//========================================




#endif /* ENCODERS_H_ */
