
#ifndef BUTTON_H_
#define BUTTON_H_

typedef enum  {

	reset,
	idle,

	button1_pressed,

	button2_pressed,

	button3_press,
	button3_up,

	button3_finger1_compress,
	button3_finger2_compress,
	button3_finger3_compress,
	button3_finger4_compress,
	button3_pastie_compress,

	button3_finger1_de_compress,
	button3_finger2_de_compress,
	button3_finger3_de_compress,
	button3_finger4_de_compress,
	button3_pastie_de_compress

}buttonstate;

void ButtonsInit( Motor * motor1, Motor * motor2, Motor * motor3, Motor * motor4, Motor * motor5);
uint8 ButtonsCheck(uint16 corner[5]);
void HandMove(Motor * motor[], int16 Power, uint16 Corner[], uint8 motorNumber);


#endif /* BUTTON_H_ */
